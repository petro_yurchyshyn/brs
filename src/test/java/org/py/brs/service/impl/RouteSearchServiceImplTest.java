package org.py.brs.service.impl;

import static org.junit.Assert.assertFalse;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.py.brs.service.impl.RouteSearchServiceImpl;

public class RouteSearchServiceImplTest {

	@InjectMocks
	RouteSearchServiceImpl routeSearchService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
    public void getRoutesStationsTest() throws Exception {
		routeSearchService.setRoutesDataFileName("bus_route_data_file.txt");
    	assertNotNull(routeSearchService.getRoutesStations());
    }
	
	@Test
	public void isValidShouldReturnTrueIfRouteLineLengthMoreThanThreeElement() throws Exception {
		String[] routeLine = {"0","0","1","2","3","4"};
		assertTrue(routeSearchService.isValidRoute(routeLine));
	}
	
	@Test
	public void isValidShouldReturnTrueIfRouteLineLengthEqualThreeElement() throws Exception {
		String[] routeLine = {"0","0","1"};
		assertTrue(routeSearchService.isValidRoute(routeLine));
	}
	
	@Test
	public void isValidShouldReturnFalseIfRouteLineLengthLessThanThreeElement() throws Exception {
		String[] routeLine = {"0","3"};
		assertFalse(routeSearchService.isValidRoute(routeLine));
	}
	    
    @Test
    public void isRouteByTwoStsShouldReturnTrueIfParamsMatchStations() throws Exception {
		int[] stations = {125, 6};
		int[][] routesStations = this.getRoutesMockData();
		
		assertTrue(routeSearchService.isRouteByTwoSts(routesStations, stations));
    }
    
    @Test
    public void isRouteByTwoStsShouldReturnFalseIfParamEqualIdOfRouteAndParamNotMatchStation() throws Exception {
		int[] stations = {0, 1};
		int[][] routesStations = this.getRoutesMockData();
		
		assertFalse(routeSearchService.isRouteByTwoSts(routesStations, stations));
    }
       
    private int[][] getRoutesMockData() {
    	int[][] routesStations = new int[3][];
    	
    	routesStations[0] = new int[]{0,12,1,17,3,4};
    	routesStations[1] = new int[]{1,125,1,6,5};
    	routesStations[2] = new int[]{2,0,6,4};
    	
		return routesStations;
    }
}