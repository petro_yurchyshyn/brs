package org.py.brs.dto;

public class RouteDTO {
	
	private int depSid;
	private int arrSid;
	private boolean directBusRoute;
	
	public RouteDTO() {
	}

	public int getDep_sid() {
		return depSid;
	}

	public void setDep_sid(int dep_sid) {
		this.depSid = dep_sid;
	}

	public int getArr_sid() {
		return arrSid;
	}

	public void setArr_sid(int arr_sid) {
		this.arrSid = arr_sid;
	}

	public boolean isDirect_bus_route() {
		return directBusRoute;
	}

	public void setDirect_bus_route(boolean direct_bus_route) {
		this.directBusRoute = direct_bus_route;
	}
}