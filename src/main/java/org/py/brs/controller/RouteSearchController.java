package org.py.brs.controller;

import org.py.brs.dto.RouteDTO;
import org.py.brs.service.RouteSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RouteSearchController {
	
	private final Logger LOG = LoggerFactory.getLogger(RouteSearchController.class);
	
	RouteSearchService routeSearchService;
	
	public RouteSearchController(RouteSearchService routeSearchService) {
		this.routeSearchService = routeSearchService;
	}

	@RequestMapping(value = "/direct", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<RouteDTO> isRoute(@RequestParam("dep_sid") int depSid, @RequestParam("arr_sid") int arrSid) {
		LOG.info("finding route");
		
		RouteDTO response = new RouteDTO();
		response.setDep_sid(depSid);
		response.setArr_sid(arrSid);
		response.setDirect_bus_route(routeSearchService.isAvailableByTwoStations(depSid, arrSid));
		
		return new ResponseEntity<RouteDTO>(response, HttpStatus.OK);
	}
}
