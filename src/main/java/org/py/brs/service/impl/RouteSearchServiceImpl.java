package org.py.brs.service.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

import org.py.brs.service.RouteSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RouteSearchServiceImpl implements RouteSearchService{
	
	private final String FOLDER_NAME = "data";
	
	@Value("${BUS_ROUTE_DATA_FILE:bus_route_data_file.txt}")
	private String routesDataFileName;
	
	private int[][] cache = null;
	
	private final Logger LOG = LoggerFactory.getLogger(RouteSearchServiceImpl.class);

	@Override
	public boolean isAvailableByTwoStations(int first, int second) {
		return this.isByTwo(first, second);
	}
	
	public void setRoutesDataFileName(String routesDataFileName) {
		this.routesDataFileName = routesDataFileName;
	}

	private boolean isByTwo(int first, int second) {
		int[] stations = {new Integer(first), new Integer(second)};
		int[][] routes = this.getRoutesStations();
		
		return this.isRouteByTwoSts(routes, stations);
	}
	
	boolean isRouteByTwoSts(int[][] routesStations, int[] stations) {
		boolean result = false;
		int first = stations[0];
		int second = stations[1];
		
		for(int[] routeStations : routesStations) {
			int[] sortedSts = toSortedStations(routeStations);
			
			int firstId = Arrays.binarySearch(sortedSts, first);
			int secondId = Arrays.binarySearch(sortedSts, second);
			
			if(firstId >= 0 & secondId >= 0){
				result = true;
				break;
			} 
		}
		return result;
	}
	
	int[][] getRoutesStations(){
		
		if(this.cache == null) {
	        File file = new File(FOLDER_NAME + "/" + routesDataFileName);
	        int numberOfRoutes = 0;
	        int[][] routesStations = null;	
	        int i = 0;
			
			try (Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(file)))) {
				 while (sc.hasNext()){
					 String[] strings = sc.nextLine().split(" ");
					 
					 if(strings.length == 1) {
						 numberOfRoutes = Integer.parseInt(strings[0]);
						 routesStations = new int[numberOfRoutes][];
					 }
					 
					 if(isValidRoute(strings)) {
						 int[] routeStations = this.toIntArray(strings);
						 routesStations[i] = routeStations;
						 i++;
					 }
		        }
	        } catch (FileNotFoundException e) {
	            LOG.error("Bus routes data file does not find: " + file.getAbsolutePath());
	            LOG.info("Please check if volume with data file configured correctly.");
	            LOG.info("Docker folder: data");
	            LOG.info("By default name of file should be: bus_route_data_file.txt");
	            LOG.info("You can use BUS_ROUTE_DATA_FILE env variable to set custom data file name");
	            
	            routesStations = null;
	        }
		
        	this.cache = routesStations;
        	LOG.info("Inserts data to cache");
        }
		
		return this.cache;
	}
	
	boolean isValidRoute(String[] routeLine) {
		return routeLine.length >= 3;
		
	}
	
	private int[] toIntArray(String[] strings) {
	    int[] converted = new int [strings.length];
	    int i=0;
	    
	    for(String str : strings){
	    	converted[i] = Integer.parseInt(str.trim());
	        i++;
	    }
	    return converted;
	}
	
	private int[] toSortedStations(int[] routeStations) {
		return Arrays.stream(routeStations).skip(1)
									       .sorted()
									       .toArray();
	}
}