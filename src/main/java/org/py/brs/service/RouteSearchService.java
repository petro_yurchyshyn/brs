package org.py.brs.service;

public interface RouteSearchService {
	boolean isAvailableByTwoStations(int depSid, int arrSid);
}
